# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kiten
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kiten/"
pkgdesc="Japanese Reference/Study Tool"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	knotifications-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kiten-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6191ba43901cd8f6c2d218ffbe74013ddd6977a47f055c4cd45d34c4ca191ce36faf200514d0da100177c1228fdfa1250a0bd87cfa95a9e98b09a1dc4c86c586  kiten-22.12.0.tar.xz
"
