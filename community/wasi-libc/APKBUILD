# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-libc
pkgver=0.20221206
_gitrev=8b7148f69ae241a2749b3defe4606da8143b72e0
pkgrel=0
pkgdesc="WASI libc implementation for WebAssembly"
url="https://github.com/WebAssembly/wasi-libc"
arch="all"
options="!check" # no testsuite
# https://github.com/WebAssembly/wasi-libc/blob/main/LICENSE
#   wasi-libc			- Apache License v2.0 WITH LLVM Exceptions AND
#				  Apache License v2.0 AND MIT
#   dlmalloc/			- CC0
#   libc-bottom-half/cloudlibc/	- BSD-2-Clause
#   libc-top-half/musl/		- MIT
license="Apache-2.0 WITH LLVM-exception AND Apache-2.0 AND MIT AND CC0-1.0 AND BSD-2-Clause"
makedepends="clang llvm"
source="wasi-libc-$_gitrev.tar.gz::https://github.com/WebAssembly/wasi-libc/archive/$_gitrev.tar.gz
	no-double-build.patch
	"
builddir="$srcdir"/$pkgname-$_gitrev

build() {
	# https://bugzilla.mozilla.org/show_bug.cgi?id=1773200#c4
	make CC=clang BULK_MEMORY_SOURCES=
}

package() {
	make INSTALL_DIR="$pkgdir"/usr/share/wasi-sysroot install
}

sha512sums="
23b89f73e12bab97205bc94c4e6977c72cad796eef170faa78f5233c9158b4673b5fd07926f8dc3db8c47e12a85fecad3b36269e091b2822c7ddb6c36b3f9971  wasi-libc-8b7148f69ae241a2749b3defe4606da8143b72e0.tar.gz
8241854f3331e4e22756f4414afcd6cd766d48e2a1a38ad6c95903f84b6f6b718b1ed0eae2ef0fe78f2ab5c87352ffb44f1eca3ea03983173b80f72af5948651  no-double-build.patch
"
