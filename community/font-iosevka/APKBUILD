# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=17.0.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
	"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
af59c8139481ed33e2cf6be8e061e5c5e0cf138878cff62ea8251c1059b019308d968e788a67ed4c0a0dc3e8775781586637481b70f2d3790c80d21602b34a1f  super-ttc-iosevka-17.0.1.zip
8a4215b5b887bb1bf055bd888e2ab1a5c4c7b2e63b92ad14eb2db3fa75b9d85b2026f8ab73bc9bade46ee127a08e01eac8167b9b8aae7324cfd5409a1908911b  super-ttc-iosevka-slab-17.0.1.zip
3cfbcfdd498f37e6def6238f34b6c2dc792ad79671399396ee2e2c1604031cf71058f697374996d5c7d85939374ca37bfc27fe1c651e38fe80478e7106237dbb  super-ttc-iosevka-curly-17.0.1.zip
4056fec5c80afd9a49bf8b5417414701ac62a77b77635fb029e4fb1a51d26f931b1479a5181451d25698bcc0058c3411a6ad4ee5f122dd2dbe3913290ddb0f28  super-ttc-iosevka-curly-slab-17.0.1.zip
"
