# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=mailimporter
pkgver=22.12.0
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kmime-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailimporter-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
89aded23be88eee9cc207af53b7b91b48a91e411bb10317f65b870286a59db36e1f482760bc7c49426ec9799998feaf0695851b06b22695c8f3dc94cd3851f41  mailimporter-22.12.0.tar.xz
"
