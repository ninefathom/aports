# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-cli-tools
pkgver=5.26.4
pkgrel=0
pkgdesc="Tools based on KDE Frameworks 5 to better interact with the system"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-cli-tools"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0-or-later AND GPL-2.0-only AND LGPL-2.1-only"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	kdesu-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kservice-dev
	kwindowsystem-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

# Workaround a circular dependency https://gitlab.alpinelinux.org/alpine/aports/-/issues/11785
install_if="plasma-workspace"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f7489f0ea2e919307bb0d9b711e67755f9a4c098edc7e2bd2342e796c56701c4917f46cb10e9e1c0c578498b5773f3bc84b5625ffc0a9b1a81b9f5d2dbd525b5  kde-cli-tools-5.26.4.tar.xz
"
