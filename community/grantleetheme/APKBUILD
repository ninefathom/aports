# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=grantleetheme
pkgver=22.12.0
pkgrel=0
pkgdesc="KDE PIM mail related libraries"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by knewstuff
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	grantlee-dev
	ki18n-dev
	knewstuff-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantleetheme-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# grantleethemetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "grantleethemetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4ada56df168906458749928d8192f40353af2ba6920dd6cb221cf3d60d8ff6e7a3d0e6b7ffb084361dd3e93f5e4bfa161f2a4e8a47e85f2d1a8e4ed5644b8df6  grantleetheme-22.12.0.tar.xz
"
