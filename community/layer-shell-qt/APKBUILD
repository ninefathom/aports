# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=layer-shell-qt
pkgver=5.26.4
pkgrel=0
pkgdesc="Qt component to allow applications to make use of the Wayland wl-layer-shell protocol"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtwayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/layer-shell-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_TESTING=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
27c7d1bdbe5ffc2c76c3ba257a3c07c964797cbedfdc0444e1c4e6eddafc9098a71bc17c9dd5c9041e4b8e76b926753b1a84dfd0f3601ca739a7efb94504ae09  layer-shell-qt-5.26.4.tar.xz
"
