# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaccounts-integration
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by signon-ui -> qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	accounts-qml-module
	signon-ui
	"
depends_dev="
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	libaccounts-qt-dev
	qt5-qtbase-dev
	samurai
	signond-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-integration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0e23fd65178ff7a5cfd2764ea25ec59b1ef8d72b68a0cbbd65bda51e6bb892bf4a4629eb7aaec19ea095f2d08934e5cafbfe4fc9ddf0ab545cc2b81c148aa584  kaccounts-integration-22.12.0.tar.xz
"
