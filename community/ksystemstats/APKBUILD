# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksystemstats
pkgver=5.26.4
pkgrel=0
pkgdesc="A plugin based system monitoring daemon"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
makedepends="
	eudev-dev
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	kio-dev
	libksysguard-dev
	libnl3-dev
	lm-sensors-dev
	networkmanager-qt-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/ksystemstats-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken, requires specific sensor setup

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4ec28658da742d1f9a5a10feb2457c52013431073a57f27925f06a0935762a86ada0bf3fbace58736a132da4d184618e976fc749b2af03cd2eae7f94fc9bddc7  ksystemstats-5.26.4.tar.xz
"
