# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-notes
pkgver=22.12.0
pkgrel=0
pkgdesc="Libraries and daemons to implement management of notes"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	ki18n-dev
	kmime-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-notes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a52ffdb68f2592cefe286f7a58c997aa3fb091865d8b5da005cfa266f5e8be44075612c03f39d1f1fb2159188c930f3aede3621c397ac2b3d8ecc7fa84e35aee  akonadi-notes-22.12.0.tar.xz
"
