# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjumpingcube
pkgver=22.12.0
pkgrel=0
pkgdesc="A simple dice driven tactical game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/kjumpingcube/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kjumpingcube-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
613d9b75f1f1554b5ddf3892b7ad7fadcae822b9d79a5e77ccb8a6c8e05df706ff4f28a67d14439b5e6ebd7698606f05705771c87e424dd5cfac62edb9c7ff11  kjumpingcube-22.12.0.tar.xz
"
