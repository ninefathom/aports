# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kio-gdrive
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KIO_GDrive"
pkgdesc="KIO Slave to access Google Drive"
license="GPL-2.0-or-later"
depends="
	kaccounts-providers
	signon-plugin-oauth2
	signon-ui
	"
makedepends="
	extra-cmake-modules
	intltool
	kaccounts-integration-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkgapi-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service//$pkgver/src/kio-gdrive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
235b97975c3daa87fb802a8457e42df0226fa0e8405b3ba41f76fa29a77ee6328b71b131e13c654065e45e2423d4302d4778f8daca8daec41e370a2907ffa563  kio-gdrive-22.12.0.tar.xz
"
