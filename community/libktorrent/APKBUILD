# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libktorrent
pkgver=22.12.0
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/org.kde.ktorrent"
pkgdesc="A powerful BitTorrent client for KDE"
license="GPL-2.0-or-later"
depends_dev="
	boost-dev
	gmp-dev
	karchive-dev
	kcrash-dev
	ki18n-dev
	kio-dev
	qca-dev
	qt5-qtbase-dev
	solid-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libktorrent-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# utppolltest requires network access
	# superseedtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(utppoll|superseed)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
63cfee87775931d50f5ac080c5d1accc741c511fdd85a2a6795fa070f4270b2bdf08e55f6795128d447d4ec6ad0e1c04bdfce0c7422d423f4d07656d81398ab5  libktorrent-22.12.0.tar.xz
"
