# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=zanshin
pkgver=22.12.0
pkgrel=1
# armhf blocked by qt5-qtdeclarative
# ppc64le and s390x blocked by qt5-qtwebengine -> akonadi-calendar
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://zanshin.kde.org/"
pkgdesc="A Getting Things Done application which aims at getting your mind like water"
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.0-or-later"
makedepends="
	akonadi-calendar-dev
	boost-dev
	extra-cmake-modules
	ki18n-dev
	kontactinterface-dev
	krunner-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="
	dbus
	xvfb-run
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/zanshin-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# tests-units-akonadi-akonadistoragetest, tests-units-akonadi-akonadicachingstorageintegrationtest,
	# tests-units-presentation-alltaskspagemodeltest, tests-units-widgets-editorviewtest,
	# tests-units-widgets-pageviewtest, tests-units-migrator-zanshin021migrationtest,
	# tests-units-migrator-zanshincontextitemsmigrationtest are broken
	local skipped_tests="("
	local tests="
		tests-units-akonadi-akonadistoragetest
		tests-units-akonadi-akonadicachingstorageintegrationtest
		tests-units-presentation-alltaskspagemodeltest
		tests-units-widgets-editorviewtest
		tests-units-widgets-pageviewtest
		tests-units-migrator-zanshin021migrationtest
		tests-units-migrator-zanshincontextitemsmigrationtest"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d7887be1e0bbafc09ee77e9fc25bd7a086599e4d66bded64c070444d701b54dab348d5a39f170f0dcdd9bfe178af10a408d3394eedd0cec82958250b059410ef  zanshin-22.12.0.tar.xz
"
