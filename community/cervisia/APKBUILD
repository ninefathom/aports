# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=cervisia
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.cervisia"
pkgdesc="A user friendly version control system front-end"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdesu-dev
	kdoctools-dev
	kiconthemes-dev
	kinit-dev
	kitemviews-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/cervisia-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9b6a1fcc9f9961aed112f4631d129d20bcf39c69a3f25fa396a0642afe70c0e5de3dcefc81e23b7bd608a40699facaf0f2a4f18d2cf5ee37dca10b3bb6744dd5  cervisia-22.12.0.tar.xz
"
