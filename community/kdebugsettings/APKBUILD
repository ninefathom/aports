# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdebugsettings
pkgver=22.12.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/utilities/"
pkgdesc="An application to enable/disable qCDebug"
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev kconfig-dev kdbusaddons-dev ki18n-dev kwidgetsaddons-dev kitemviews-dev kcompletion-dev samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdebugsettings-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
a822031d2a065bd42e82980ffd020795639b3045b9e09e7edb7271e32dd8a3a8ae4e1429d46d5a5e82e6cc9401622bc7f161d8382204e8964d1f5094de86d6ca  kdebugsettings-22.12.0.tar.xz
"
