# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khelpcenter
pkgver=22.12.0
pkgrel=0
pkgdesc="Application to show KDE Applications' documentation"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/KHelpCenter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kinit-dev
	kservice-dev
	kwindowsystem-dev
	libxml2-dev
	qt5-qtbase-dev
	samurai
	xapian-core-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
eb5b824069cc4d884108cca203e4575d0d352149053cc9dd3f459d93dac34f1fe183a1ab72fb8cd976d001326bb718276f13018d9a2c42e0ed79c3798e2f4168  khelpcenter-22.12.0.tar.xz
"
