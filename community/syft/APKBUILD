# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.64.0
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86 !ppc64le !riscv64" # FTBFS on 32-bit arches, riscv64, ppc64le
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X github.com/anchore/syft/internal/version.version=$pkgver
		" \
		-o bin/syft ./cmd/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft -t "$pkgdir"/usr/bin/
}

sha512sums="
d223c54467dadcae98828499da008e1119de585a8b54f20fe0fc90a85bbbf4dbaebdf4a38016c97108a16699151d1fb204f6eba3a3c3339aceb000983f6de95b  syft-0.64.0.tar.gz
"
