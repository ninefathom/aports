# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-gtk-config
pkgver=5.26.4
pkgrel=0
pkgdesc="GTK2 and GTK3 Configurator for KDE"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-gtk-config"
license="GPL-2.0 AND LGPL-2.1-only OR LGPL-3.0-only"
depends="gsettings-desktop-schemas"
makedepends="
	extra-cmake-modules
	gsettings-desktop-schemas-dev
	gtk+2.0-dev
	gtk+3.0-dev
	karchive-dev
	kcmutils-dev
	kconfigwidgets-dev
	kdecoration-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f6fe8e4edbc525c3effee9ecd05b6209dd63dfa4226ce8bdde5c8174ee460d475323192d877e0fe7c77975ec195ab1911f96f8ff1e2a12329d34ba8388c97c3f  kde-gtk-config-5.26.4.tar.xz
"
