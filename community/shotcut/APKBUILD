# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=shotcut
pkgver=22.11.25
pkgrel=0
pkgdesc="Cross-platform video editor"
url="https://www.shotcut.org"
arch="all !armhf !armv7 !aarch64" # no opengl1.1 support, only 2
license="GPL-3.0-or-later"
depends="qt5-qtquickcontrols"
makedepends="
	cmake
	ffmpeg-dev
	fftw-dev
	mlt-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qttools-dev
	qt5-qtwebsockets-dev
	qt5-qtx11extras-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.xz::https://github.com/mltframework/shotcut/releases/download/v$pkgver/shotcut-src-${pkgver//./}.txz"
builddir="$srcdir/src/shotcut"
options="!check" # No test suite present

build() {
	export CXXFLAGS="$CXXFLAGS -DSHOTCUT_NOUPGRADE"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

lang() {
	pkgdesc="Languages for package shotcut"
	install_if="$pkgname=$pkgver-r$pkgrel lang"
	amove usr/share/shotcut/translations
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c40bf9b599c51bb96448cbb8a0a2942ea1144845292b02ba3ebd0572056df9fa6e92cc8650f148388f17944cbeea279907a57b136a988cc543cead419c3812a3  shotcut-22.11.25.tar.xz
"
