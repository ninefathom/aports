# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksmtp
pkgver=22.12.0
pkgrel=0
pkgdesc="Job-based library to send email through an SMTP server"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	cyrus-sasl-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksmtp-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bc0c27788c33540e88da809fb3d3f9cc819cac5fe716e22aad431a2253c28ca3ab1f0826c9cc8df437541be49beeb4853c5e1004bd2be0c90409bccd426f3115  ksmtp-22.12.0.tar.xz
"
