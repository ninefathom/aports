# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=lokalize
pkgver=22.12.0
pkgrel=0
pkgdesc="Computer-Aided Translation System"
url="https://apps.kde.org/lokalize/"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	hunspell-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kross-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	sonnet-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/lokalize-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0d8bc05a92d1c6e663c33ffad219a0e71e6c574e9b36c6a877d251620ec56e8c4cf6bd9ebc48b2c44a41a73d295810f3b0e2e457082b478f2d355187e7382fb3  lokalize-22.12.0.tar.xz
"
