# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpimtextedit
pkgver=22.12.0
pkgrel=0
pkgdesc="Advanced text editor which provide advanced html feature"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://api.kde.org/kdepim/kpimtextedit/html"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	grantlee-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kemoticons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtspeech-dev
	samurai
	sonnet-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpimtextedit-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	cd build

	# kpimtextedit-plaintext-textgotolinewidgettest,
	# kpimtextedit-composerng-richtextcomposertest and
	# kpimtextedit-composerng-richtextcomposercontrolertest require OpenGL
	# kpimtextedit-texttospeech-texttospeechwidgettest requires texttospeech
	# kpimtextedit-texttospeech-texttospeechactionstest and
	# kpimtextedit-grantleebuilder-texthtmlbuildertest are broken
	local skipped_tests="kpimtextedit-("
	local tests="
		plaintext-textgotolinewidget
		texttospeech-texttospeechwidget
		texttospeech-texttospeechactions
		composerng-richtextcomposer
		composerng-richtextcomposercontroler
		grantleebuilder-texthtmlbuilder
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
fc8610d414bb2fa7a804d73c23e5fcae8a72bd69d5f54b6df4eaa27a6181c17c49eb841cd12a686d05affe73091bbbb36a182db68b8a0e04f0e210e238e0bdef  kpimtextedit-22.12.0.tar.xz
"
