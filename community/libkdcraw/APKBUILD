# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkdcraw
pkgver=22.12.0
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org"
pkgdesc="RAW image file format support for KDE"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	libraw-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdcraw-$pkgver.tar.xz
	libraw-0.21.patch
	"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
77a4680a7e5dd5b3a5c9b9a982c4e3e6300a42ce019b84c9c2b96908b28a1549869b4796cff1f6813db0174a956ad3ae39b8c6c6593c3a877ec3691e5bca4a8e  libkdcraw-22.12.0.tar.xz
b343bfd66423e51aa20e38e72c42738c1aec1f9cbf002101c44d156a90289cdff2596890925c341553c39f236ec2e28ca944316c16d677c97ba0c0de0acb14b3  libraw-0.21.patch
"
