# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=artikulate
pkgver=22.12.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://edu.kde.org/artikulate"
pkgdesc="Improve your pronunciation by listening to native speakers"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kirigami2-dev
	knewstuff-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/artikulate-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e33e7cdb8afb51c079b6966273d29ff9cda25dd0a897a8ef4c053d285f152ea20f30458cfde4fb29e7761343379d7d3acfcf57944d55ac9db20ad09c54692250  artikulate-22.12.0.tar.xz
"
