# Contributor: Kaspar Schleiser <kaspar@schleiser.de>
# Maintainer: Kaspar Schleiser <kaspar@schleiser.de>
pkgname=laze
pkgver=0.1.11
pkgrel=0
pkgdesc="laze is a build system based on Ninja, aiming to be the next goto-alternative to make"
url="https://laze-build.org"
arch="all"
license="Apache-2.0"
depends="ninja"
makedepends="cargo"
checkdepends="xz"
subpackages="$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/kaspar030/laze/archive/$pkgver/laze-$pkgver.tar.gz"

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 "target/release/laze" "$pkgdir/usr/bin/laze"

	# create man pages
	mkdir -p tmp/man tmp/completions
	target/release/laze manpage tmp/man

	install -Dm644 "tmp/man/laze.1" "$pkgdir/usr/share/man/man1/laze.1"
	install -Dm644 "tmp/man/laze-build.1" "$pkgdir/usr/share/man/man1/laze-build.1"
	install -Dm644 "tmp/man/laze-clean.1" "$pkgdir/usr/share/man/man1/laze-clean.1"
	install -Dm644 "tmp/man/laze-task.1" "$pkgdir/usr/share/man/man1/laze-task.1"

	# create completions
	for shell in bash zsh fish; do
		target/release/laze -Ctmp/completions completion --generate $shell \
			> tmp/completions/$shell
	done

	install -Dm644 "tmp/completions/bash" \
		"$pkgdir/usr/share/bash-completion/completions/laze"
	install -Dm644 "tmp/completions/fish" \
		"$pkgdir/usr/share/fish/completions/laze.fish"
	install -Dm644 "tmp/completions/zsh" \
		"$pkgdir/usr/share/zsh/site-functions/_laze"
}

sha512sums="
0321b4e8b84115b9bfb788d8d56f872cdd7f18c46b9ba01d71f706391e6db41634731787c5ba22ee1d89d1ac35fa6cb76c8e9dbd531998c315ef54a0afb3993e  laze-0.1.11.tar.gz
"
