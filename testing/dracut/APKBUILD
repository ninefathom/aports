# Contributor: Laszlo Gombos <laszlo.gombos@gmail.com>
# Maintainer: Conrad Hoffmann <ch@bitfehler.net>
pkgname=dracut
pkgver=057
pkgrel=7
pkgdesc="An event driven initramfs infrastructure"
url="https://github.com/dracutdevs/dracut/wiki"
arch="all !riscv64" # textrel
license="GPL-2.0-or-later"
makedepends="asciidoc musl-fts-dev kmod-dev bash coreutils blkid findmnt eudev"
subpackages="$pkgname-modules-network:network $pkgname-modules:modules $pkgname-dev $pkgname-doc $pkgname-bash-completion $pkgname-tests $pkgname-core:core"
options="!check" # There is a test suite, but it requires root/sudo
triggers="$pkgname.trigger=/usr/share/kernel/*"
source="$pkgname-$pkgver.tar.gz::https://github.com/dracutdevs/dracut/archive/refs/tags/$pkgver.tar.gz"
provides="initramfs-generator"
provider_priority=100 # low, somewhat experimental

build() {
	./configure --sysconfdir="/etc"
	CFLAGS="$CFLAGS -D__GLIBC_PREREQ=" CWD="$(pwd)" make
}

package() {
	DESTDIR="$pkgdir" make install
	depends="$pkgname-core $pkgname-modules-network"
	rm -rf test/container test/TEST*SYSTEMD* test/TEST*RPM*
	mv test "$pkgdir"/usr/lib/dracut/
}

network() {
	pkgdesc="network dracut modules"
	depends="dracut-modules dhclient iputils"

	for f in 35network-legacy 40network 90livenet \
		95fcoe 95iscsi 95nbd 95nfs; do
		amove usr/lib/dracut/modules.d/$f
	done

	if [ "$CARCH" = "s390x" ]; then
		amove usr/lib/dracut/modules.d/95zfcp
		amove usr/lib/dracut/modules.d/95znet
	fi
}

modules() {
	pkgdesc="local dracut modules"
	depends="dracut-core eudev util-linux-misc"
	rm -rf "$pkgdir"/usr/lib/dracut/modules.d/*systemd*
	rm -rf "$pkgdir"/usr/share/man/man8/*.service.*
	rm -rf "$pkgdir"/usr/lib/kernel

	# systemd dependent additional dracut modules
	for f in 06rngd 06dbus-broker 06dbus-daemon 09dbus 35connman \
		35network-manager 35network-wicked 62bluetooth 80lvmmerge \
		91fido2 91pcsc 91pkcs11 91tpm2-tss 99memstrack 99squash; do
		rm -rf "$pkgdir"/usr/lib/dracut/modules.d/$f
	done

	if [ "$CARCH" != "s390x" ]; then
		for f in 80cms 81cio_ignore 91zipl 95dasd 95dasd_mod \
			95dasd_rules 95dcssblk 95qeth_rules 95zfcp \
			95zfcp_rules 95znet; do
			rm -rf "$pkgdir"/usr/lib/dracut/modules.d/$f
		done
	else
		rm -rf "$pkgdir"/usr/lib/dracut/modules.d/00warpclock
	fi

	amove usr/lib/dracut/modules.d
}

core() {
	pkgdesc="core tools for dracut"
	depends="bash coreutils blkid findmnt"
	amove etc usr
}

tests() {
	pkgdesc="dracut tests"
	depends="dracut-modules e2fsprogs grep make qemu-img qemu-system-x86_64 sfdisk strace sudo"
	amove usr/lib/dracut/test
}

sha512sums="
8acdc8db2233a9abbaeea218cc5b1be68c4985088995f42624750783f8d40ecbb7fa97ab4f6468f67c079c8418590ace317c143a92d9305640b48c7c0edd4089  dracut-057.tar.gz
"
