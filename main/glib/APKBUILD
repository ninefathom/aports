# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=glib
pkgver=2.74.3
pkgrel=2
pkgdesc="Common C routines used by Gtk+ and other libs"
url="https://developer.gnome.org/glib/"
arch="all"
license="LGPL-2.1-or-later"
triggers="$pkgname.trigger=/usr/share/glib-2.0/schemas:/usr/lib/gio/modules:/usr/lib/gtk-4.0"
depends_dev="
	bzip2-dev
	docbook-xml
	docbook-xsl
	gettext-dev
	libxml2-utils
	libxslt
	python3
	"
makedepends="$depends_dev pcre2-dev meson zlib-dev libffi-dev util-linux-dev"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-static $pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/glib/${pkgver%.*}/glib-$pkgver.tar.xz
	0001-gquark-fix-initialization-with-c-constructors.patch
	deprecated-no-warn.patch
	0001-gslice-remove-slice-allocator.patch
	gvariant-normal-form-handling.patch
	disallow-exporting-large-menus-on-bus.patch
	"
options="!check" # don't like to be run without first being installed

# secfixes:
#   2.66.6-r0:
#     - CVE-2021-27219 GHSL-2021-045
#   2.62.5-r0:
#     - CVE-2020-6750
#   2.60.4-r0:
#     - CVE-2019-12450

build() {
	abuild-meson \
		--default-library=both \
		-Dman=true \
		-Dtests="$(want_check && echo true || echo false)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/share
		find "$pkgdir"/usr/bin ! -name "glib-compile-schemas" -a \( \
		-name "gdbus-codegen" -o \
		-name "gobject-query" -o \
		-name "gresource" -o \
		-name "gtester*" -o \
		-name "glib-*" \) \
		-exec mv {} "$subpkgdir"/usr/bin \;

	amove usr/share/gdb usr/share/glib-2.0
}

static() {
	default_static
	depends="gettext-static"
}

sha512sums="
a9aa7e84187abb57aeeff9c7f4c4125be742a510ae5d39b6b62696ad1a715c36b353c6c14222caeb1e87bed930fb54184dba77118b991c42f1857a292c6aa77b  glib-2.74.3.tar.xz
32e5aca9a315fb985fafa0b4355e4498c1f877fc1f0b58ad4ac261fb9fbced9f026c7756a5f2af7d61ce756b55c8cd02811bb08df397040e93510056f073756b  0001-gquark-fix-initialization-with-c-constructors.patch
744239ea2afb47e15d5d0214c37d7c798edac53797ca3ac14d515aee4cc3999ef9716ba744c64c40198fb259edc922559f77c9051104a568fc8ee4fc790810b1  deprecated-no-warn.patch
b47f1330d1747aac74eec5cf670f43084471acebc2f455e1c3374e3d642e7cdf415e11fc6c3910681f4c422d9bfff66f0aa96935c28d282c3a38faaf4271da68  0001-gslice-remove-slice-allocator.patch
167b2ee5cbbbb153d1f9f94b7645ecf41294e9468dc1701cc7d23dca8edb5e0e2f857e5ff7b3b4a18e1a4ce0bc02ae43eef1debfe1ec357e8a57a06bc96ad089  gvariant-normal-form-handling.patch
3c5d3667efcff9ca88a63f2208c9ab031a77734fb65dc91b669112a43d2130bf9aec43b249999a2899e5ed4174385ab2bd8168fc229b920f2dcdc1cb76e8d5fa  disallow-exporting-large-menus-on-bus.patch
"
